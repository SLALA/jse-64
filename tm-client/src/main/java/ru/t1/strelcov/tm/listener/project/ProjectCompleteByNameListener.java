package ru.t1.strelcov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.request.ProjectChangeStatusByNameRequest;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.util.TerminalUtil;

import static ru.t1.strelcov.tm.enumerated.Status.COMPLETED;

@Component
public final class ProjectCompleteByNameListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-complete-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Complete project by name.";
    }

    @Override
    @EventListener(condition = "@projectCompleteByNameListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[COMPLETE PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = projectEndpoint.changeStatusByNameProject(new ProjectChangeStatusByNameRequest(getToken(), name, COMPLETED.name())).getProject();
        showProject(project);
    }

}
