package ru.t1.strelcov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.response.UserLoginResponse;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public final class UserLoginListener extends AbstractUserListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login.";
    }

    @Override
    @EventListener(condition = "@userLoginListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest(login, password));
        Optional.ofNullable(response.getToken()).filter((i) -> !i.isEmpty()).orElseThrow(AccessDeniedException::new);
        setToken(response.getToken());
        System.out.println("Session Token: " + response.getToken());
    }

}
