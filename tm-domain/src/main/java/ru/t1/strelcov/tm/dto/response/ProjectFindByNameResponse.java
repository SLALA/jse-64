package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;

@Setter
@Getter
@NoArgsConstructor
public final class ProjectFindByNameResponse extends AbstractProjectResponse {

    public ProjectFindByNameResponse(@NotNull final ProjectDTO project) {
        super(project);
    }

}
