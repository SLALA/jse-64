package ru.t1.strelcov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IDataEndpoint {

    @NotNull String SPACE = "http://endpoint.tm.strelcov.t1.ru/";
    @NotNull String PART = "DataEndpointService";
    @NotNull String NAME = "DataEndpoint";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDataEndpoint newInstance(@NotNull final String host, @NotNull final Integer port) {
        @NotNull final String wsdlURL = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdlURL);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(IDataEndpoint.class);
    }

    @WebMethod
    @NotNull
    DataBase64LoadResponse loadBase64Data(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64LoadRequest request);

    @WebMethod
    @NotNull
    DataBase64SaveResponse saveBase64Data(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64SaveRequest request);

    @WebMethod
    @NotNull
    DataBinaryLoadResponse loadBinaryData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinaryLoadRequest request);

    @WebMethod
    @NotNull
    DataBinarySaveResponse saveBinaryData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinarySaveRequest request);

    @WebMethod
    @NotNull
    DataJsonJAXBSaveResponse saveJsonJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonJAXBSaveRequest request);

    @WebMethod
    @NotNull
    DataJsonJAXBLoadResponse loadJsonJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonJAXBLoadRequest request);

    @WebMethod
    @NotNull
    DataJsonFasterXMLLoadResponse loadJsonFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonFasterXMLLoadRequest request);

    @WebMethod
    @NotNull
    DataXmlFasterXMLLoadResponse loadXmlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlFasterXMLLoadRequest request);

    @WebMethod
    @NotNull
    DataYamlFasterXMLSaveResponse saveYamlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlFasterXMLSaveRequest request);

    @WebMethod
    @NotNull
    DataJsonFasterXMLSaveResponse saveJsonFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonFasterXMLSaveRequest request);

    @WebMethod
    @NotNull
    DataXmlFasterXMLSaveResponse saveXmlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlFasterXMLSaveRequest request);

    @WebMethod
    @NotNull
    DataYamlFasterXMLLoadResponse loadYamlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlFasterXMLLoadRequest request);

    @WebMethod
    @NotNull
    DataXmlJAXBLoadResponse loadXmlJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlJAXBLoadRequest request);

    @WebMethod
    @NotNull
    DataXmlJAXBSaveResponse saveXmlJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlJAXBSaveRequest request);

}
