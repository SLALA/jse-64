<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>

<h1>task List</h1>

<table width="100%" cellpadding="10" border="1" style="margin-top: 20px">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Status</th>
        <th>Created</th>
        <th>Started</th>
        <th>Project Id</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td>
                <c:out value="${task.status.displayName}"/>
            </td>
            <td>
                <c:out value="${task.created}"/>
            </td>
            <td>
                <c:out value="${task.dateStart}"/>
            </td>
            <td>
                <c:out value="${task.projectId}"/>
            </td>
            <td align="center">
                <a href="/task/edit/${task.id}">Edit</a>
            </td>
            <td align="center">
                <a href="/task/remove/${task.id}">Remove</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" method="post" style="margin-top: 20px">
    <button>Create</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
