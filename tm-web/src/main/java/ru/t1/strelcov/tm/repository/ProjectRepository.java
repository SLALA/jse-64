package ru.t1.strelcov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.strelcov.tm.model.Project;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProjectRepository {

    @NotNull
    private static final Map<String, Project> projects = new LinkedHashMap<>();

    {
        add("Project 1", "Project created.");
    }

    public void add(@NotNull final String name) {
        @NotNull final Project project = new Project(name);
        projects.put(project.getId(), project);
    }

    public void add(@NotNull final String name, @Nullable final String description) {
        @NotNull final Project project = new Project(name, description);
        projects.put(project.getId(), project);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

    @NotNull
    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

}