package ru.t1.strelcov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.repository.ProjectRepository;
import ru.t1.strelcov.tm.repository.TaskRepository;

@Controller
@RequestMapping("/task")
public class TaskController {

    @NotNull
    @Autowired
    TaskRepository taskRepository;

    @NotNull
    @Autowired
    ProjectRepository projectRepository;

    @GetMapping("/list")
    public ModelAndView list() {
        return new ModelAndView("task-list", "tasks", taskRepository.findAll());
    }

    @PostMapping("/create")
    public String add() {
        int number = taskRepository.findAll().size() + 1;
        taskRepository.add("Task " + number);
        return "redirect:/task/list";
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(
            @NotNull @PathVariable("id") final String id) {
        @Nullable final Task task = taskRepository.findById(id);
        if (task == null) return list();
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectRepository.findAll());
        return modelAndView;
    }

    @NotNull
    @PostMapping("/edit/{id}")
    public String edit(
            @NotNull @ModelAttribute("task") final Task task,
            @NotNull final BindingResult result
    ) {
        taskRepository.save(task);
        return "redirect:/task/list";
    }

    @GetMapping("/remove/{id}")
    public String remove(
            @NotNull @PathVariable("id") final String id) {
        taskRepository.removeById(id);
        return "redirect:/task/list";
    }

}
