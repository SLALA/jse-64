package ru.t1.strelcov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.strelcov.tm.model.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TaskRepository {

    @NotNull
    private final Map<String, Task> Tasks = new LinkedHashMap<>();

    {
        add("Task 1", "Task created.");
    }

    public void add(@NotNull final String name) {
        @NotNull final Task Task = new Task(name);
        Tasks.put(Task.getId(), Task);
    }

    public void add(@NotNull final String name, @Nullable final String description) {
        @NotNull final Task Task = new Task(name, description);
        Tasks.put(Task.getId(), Task);
    }

    public void removeById(@NotNull final String id) {
        Tasks.remove(id);
    }

    @NotNull
    public List<Task> findAll() {
        return new ArrayList<>(Tasks.values());
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return Tasks.get(id);
    }

    public void setProjectId(@NotNull final String id, @NotNull final String projectId) {
        Tasks.get(id).setProjectId(projectId);
    }

    public void save(@NotNull final Task Task) {
        Tasks.put(Task.getId(), Task);
    }

}