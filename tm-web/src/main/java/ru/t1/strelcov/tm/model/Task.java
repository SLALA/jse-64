package ru.t1.strelcov.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.strelcov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

import static ru.t1.strelcov.tm.enumerated.Status.NOT_STARTED;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    @Nullable
    private String description;

    @Nullable
    private String projectId;

    @NotNull
    private Status status = NOT_STARTED;

    @NotNull
    private Date created = new Date();

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date dateStart;

    public Task(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

    public Task(@NotNull String name) {
        this.name = name;
    }
}
