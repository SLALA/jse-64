package ru.t1.strelcov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.repository.ProjectRepository;

@Controller
@RequestMapping("/project")
public class ProjectController {

    @NotNull
    @Autowired
    ProjectRepository projectRepository;

    @GetMapping("/list")
    public ModelAndView list() {
        return new ModelAndView("project-list", "projects", projectRepository.findAll());
    }

    @PostMapping("/create")
    public String add() {
        int number = projectRepository.findAll().size() + 1;
        projectRepository.add("Project " + number);
        return "redirect:/project/list";
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(
            @NotNull @PathVariable("id") final String id) {
        @Nullable final Project project = projectRepository.findById(id);
        if (project == null) return list();
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @NotNull
    @PostMapping("/edit/{id}")
    public String edit(
            @NotNull @ModelAttribute("project") final Project project,
            @NotNull final BindingResult result
    ) {
        projectRepository.save(project);
        return "redirect:/project/list";
    }

    @GetMapping("/remove/{id}")
    public String remove(
            @NotNull @PathVariable("id") final String id) {
        projectRepository.removeById(id);
        return "redirect:/project/list";
    }

}
