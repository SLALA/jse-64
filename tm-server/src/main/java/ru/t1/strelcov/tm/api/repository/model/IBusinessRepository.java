package ru.t1.strelcov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.List;

@NoRepositoryBean
public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    @NotNull
    List<E> findAllByUserId(@NotNull final String userId);

    @NotNull
    List<E> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @Nullable
    E findFirstByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    void deleteByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    E findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserId(@NotNull final String userId);

}
